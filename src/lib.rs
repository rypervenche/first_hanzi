pub mod constants;

use anyhow::{anyhow, Result};
use clap::Parser;
pub use constants::cns2component::CNS2COMPONENT;
pub use constants::cns2unicode::CNS2UNICODE;
pub use constants::frequency::FREQUENCY;
pub use std::collections::{HashMap, HashSet};

const MAX_PER_COMPONENT: usize = 3;
pub const MAX_TOTAL: usize = 3000;

/// Command line options
#[derive(Parser, Debug)]
#[clap(version, about = "Find most efficient first hanzi to learn.")]
pub struct Opts {
    #[clap(short = 'c', long)]
    pub with_previous_components: bool,
}

#[derive(Debug, Clone)]
pub struct Character {
    pub character: char,
    pub unicode: String,
    pub cns: String,
    pub components: Variants,
    pub frequency: usize,
}

#[derive(Debug, Clone)]
pub struct Variants {
    variants: Vec<Variant>,
}

#[derive(Debug, Clone)]
pub struct Variant {
    components: Vec<String>,
}

impl Character {
    pub fn new(cns: &str) -> Option<Self> {
        let unicode = CNS2UNICODE
            .get(cns)
            .expect("No character found.")
            .to_string();
        let character = hex2char(&unicode);
        let cns = cns.to_string();
        let frequency = match get_frequency(&character).ok() {
            Some(f) => f,
            None => return None,
        };
        let components = get_variants(&cns);

        Some(Character {
            character,
            unicode,
            cns,
            components,
            frequency,
        })
    }
}

#[derive(Debug, Clone)]
pub struct Characters {
    pub characters: HashMap<char, Character>,
}

impl Characters {
    pub fn new() -> Self {
        let mut characters = HashMap::new();
        for cns in CNS2COMPONENT.keys() {
            let character = match Character::new(cns) {
                Some(c) => c,
                None => continue,
            };
            characters.insert(character.character, character);
        }
        Characters { characters }
    }

    pub fn count_table(&self) -> HashMap<String, usize> {
        let mut count: HashMap<String, usize> = HashMap::new();
        for character in self.characters.values() {
            for variant in &character.components.variants {
                for component in &variant.components {
                    *count.entry(component.to_owned()).or_default() += 1;
                }
            }
        }
        count
    }

    pub fn components(count: HashMap<String, usize>) -> Vec<String> {
        let mut sorted: Vec<_> = count.iter().collect();
        sorted.sort_by_key(|a| a.1);
        let components = sorted.iter().rev().map(|(a, _)| a.to_string()).collect();
        components
    }
}

impl Default for Characters {
    fn default() -> Self {
        Self::new()
    }
}

pub fn hex2char(hex: &str) -> char {
    let unicode = u32::from_str_radix(hex, 16).expect("Bad hex code point.");
    char::from_u32(unicode).expect("Can't convert u32 to char.")
}

pub fn get_frequency(hanzi: &char) -> Result<usize> {
    let num = FREQUENCY
        .get(&hanzi.to_string())
        .ok_or_else(|| anyhow!("Bad frequency"))?;
    Ok(num.parse::<usize>().unwrap())
}

pub fn get_variants(cns: &str) -> Variants {
    let string = CNS2COMPONENT.get(cns).expect("Components not found.");
    let variants: Vec<&str> = string.split(';').collect();
    let variants: Vec<Variant> = variants
        .iter()
        .map(|v| {
            let components: Vec<_> = v.split(',').map(str::to_string).collect();
            Variant { components }
        })
        .collect();
    let components: Variants = Variants { variants };
    components
}

pub fn most_common(
    opts: &Opts,
    characters: &Characters,
    components: &Vec<String>,
) -> Vec<Character> {
    let mut list: Vec<Character> = vec![];
    let mut component_set = HashSet::new();

    for component in components {
        let mut chars_with_comp =
            find_chars_with_component(opts, characters, component, &component_set);
        component_set.insert(component.clone());
        chars_with_comp.sort_by_key(|c| c.frequency);
        if chars_with_comp.len() >= MAX_PER_COMPONENT {
            chars_with_comp = chars_with_comp[..MAX_PER_COMPONENT].to_vec();
        }
        list.extend(chars_with_comp);
    }
    if list.len() > MAX_TOTAL {
        list[..MAX_TOTAL].to_vec()
    } else {
        list
    }
}

fn find_chars_with_component(
    opts: &Opts,
    characters: &Characters,
    component: &str,
    component_set: &HashSet<String>,
) -> Vec<Character> {
    let mut found_chars: Vec<Character> = vec![];
    for character in characters.characters.values() {
        if component_in_character(opts, character, component, component_set) {
            found_chars.push(character.to_owned());
        }
    }
    found_chars
}

fn component_in_character(
    opts: &Opts,
    character: &Character,
    component: &str,
    component_set: &HashSet<String>,
) -> bool {
    for components in &character.components.variants {
        if components.components.contains(&component.to_string()) {
            if !opts.with_previous_components {
                return true;
            } else if !component_set.is_empty() {
                for comp in component_set {
                    if components.components.contains(comp) {
                        return true;
                    }
                }
            }
        }
    }
    false
}
