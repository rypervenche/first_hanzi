use clap::Parser;
use first_hanzi::*;

// TODO:
// - Change HashSet to Vec and keep order, search by most common components (and also try by least
// common
// - Check to see if output is consisent with each run
// - Skip character if it's already been used
// - Add option to use or not use component_set
// - Find all characters with that component in it, AS WELL with any of the previous components in the set
//   if there aren't  results, then redo without component_set (keep any found characters though)
//   then sort by frequency and "head" the results"
fn main() {
    let opts = Opts::parse();
    let characters = Characters::new();
    let component_counts = characters.count_table();
    let components = Characters::components(component_counts);

    let list = most_common(&opts, &characters, &components);

    let len = list.len();
    let mut state = 0;
    for component in components {
        println!("Component: {}", component);
        for _ in 0..3 {
            if state < len {
                println!("{}", list[state].character);
                state += 1;
            }
        }
    }
}
