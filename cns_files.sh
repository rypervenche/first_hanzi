#!/usr/bin/env bash

dir="./src/constants"
temp_dir="./tmp"
mkdir $temp_dir
mkdir -p $dir

# Get files
sort -n ~/下載/Open_Data/MapingTables/Unicode/* > $temp_dir/cns2unicode.txt
cp ~/下載/Open_Data/Properties/CNS_component.txt $temp_dir/
cp ./resources/most-common-characters.csv $temp_dir/

# cns2unicode
echo "use phf::phf_map;

pub static CNS2UNICODE: phf::Map<&'static str, &'static str> = phf_map! {" > $dir/cns2unicode.rs

sed -E 's/([A-Z0-9-]+)\s+([A-Z0-9]+)/    "\1" => "\2",/' $temp_dir/cns2unicode.txt >> $dir/cns2unicode.rs

echo "};" >> $dir/cns2unicode.rs

# CNS_components
echo "use phf::phf_map;

pub static CNS2COMPONENT: phf::Map<&'static str, &'static str> = phf_map! {" > $dir/cns2component.rs

sed -E 's/([^\t]+)\t(.+)/    "\1" => "\2",/' $temp_dir/CNS_component.txt >> $dir/cns2component.rs

echo -e "\n};" >> $dir/cns2component.rs

# most-common-characters
echo "use phf::phf_map;

pub static FREQUENCY: phf::Map<&'static str, &'static str> = phf_map! {" > $dir/frequency.rs

sed -E 's/([^,]+),(.+)/    "\2" => "\1",/' $temp_dir/most-common-characters.csv >> $dir/frequency.rs

echo "};" >> $dir/frequency.rs

# Cleanup
rm -rf $temp_dir
